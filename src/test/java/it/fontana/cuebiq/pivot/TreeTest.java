//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

package it.fontana.cuebiq.pivot;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.Test;

import it.fontana.cuebiq.pivot.tree.Leaf;
import it.fontana.cuebiq.pivot.tree.Node;
import it.fontana.cuebiq.pivot.tree.Tree;
import it.fontana.cuebiq.pivot.tree.visitor.EvaluationVisitor;

public class TreeTest extends CommonTest {

	@Test
	public void testInsertSearch() {
		String key = "grand total";
		Tree aggregation = new Node(key,0);
		assertEquals(key, aggregation.getKey());
		
		aggregation.insert(Arrays.asList("Germany","Green","Brown"), 168);
		
		Optional<Tree> germanySubTree = aggregation.getChild("Germany");
		assertTrue(germanySubTree.isPresent());
		
		Optional<Tree> greenSubTree = aggregation.getChild("Green");
		assertFalse(greenSubTree.isPresent());
		
		greenSubTree = germanySubTree.get().getChild("Green");
		assertTrue(greenSubTree.isPresent());
		
		Optional<Tree> brownSubTree = greenSubTree.get().getChild("Brown");
		assertTrue(brownSubTree.isPresent());
		assertTrue("brownSubTree is a Leaf", brownSubTree.get() instanceof Leaf);
		
		Leaf brownLeaf = (Leaf) brownSubTree.get();
		assertTrue(brownLeaf.getValues().size() == 1);
		assertTrue(brownLeaf.getValues().get(0) == 168);
		
		
		aggregation.insert(Arrays.asList("Spain","Green","Brown"), 359);
		
		Optional<Tree> spainSubTree = aggregation.getChild("Spain");
		assertTrue(spainSubTree.isPresent());
		
		aggregation.insert(Arrays.asList("Germany","Green","Red"), 536);
		aggregation.insert(Arrays.asList("Germany","Green","Red"), 906);
		
		Optional<List<Integer>> redValues = greenSubTree.flatMap( g -> g.getChild("Red")).map( r -> (Leaf) r).map(l -> l.getValues());
		
		assertTrue(redValues.isPresent());
		
		assertEquals(redValues.get().size(), 2);
		assertTrue("second element is 200",redValues.get().get(1) == 906);
		
		aggregation.insert(Arrays.asList("Germany","Blue","Brown"), 389);

		
		Optional<Tree> result = aggregation.search(Arrays.asList("dads"));
		assertTrue("dads node does not exist",!result.isPresent());
		
		result = aggregation.search(Arrays.asList("Germany"));
		
		assertTrue("Germany node exists",result.isPresent());
		
		result = aggregation.search(Arrays.asList("Germany","dads"));
		
		assertTrue("Germany->dads node does not exist",!result.isPresent());
		
		result = aggregation.search(Arrays.asList("Germany","Green","Red"));
		
		assertTrue("Germany->Green->Red tree exist",result.isPresent());
		
		assertTrue("Germany->Green->Red tree is a leaf",result.get() instanceof Leaf);
		
		Leaf redLeaf = (Leaf) result.get();
		
		assertEquals(redLeaf.getValues(), Arrays.asList(536,906));
		
		result = aggregation.search(Arrays.asList("Germany","Green"));
		
		
		assertTrue("Germany->Green tree exist",result.isPresent());
		
		assertTrue("Germany->Green tree is a node",result.get() instanceof Node);
		
		
		
	}
	
	
	@Test
	public void visitorTest() {
		String key = "grand total";
		Tree data = new Node(key,0);
		List<String> nodesGGB = Arrays.asList("Germany","Green","Brown");
		List<Integer> values = Arrays.asList(168,200);
		values.stream().forEach( value -> data.insert(nodesGGB, value));
		
		
		assertTrue("evaluation is null on not evaluated tree",!data.getEvaluation().isPresent());
		
		data.accept(new EvaluationVisitor(sum));
		assertTrue("evaluation is not null after evaluation",data.getEvaluation().isPresent());
		
		int sumValues = values.stream().mapToInt(Integer::intValue).sum();
		
		assertEquals(data.getEvaluation().get().getValue(), sumValues);
		
		assertEquals(data.search(nodesGGB.subList(0, 0)).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1)),new  Evaluation(values.size(),sumValues));
		assertEquals(data.search(nodesGGB.subList(0, 1)).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1)),new  Evaluation(values.size(),sumValues));
		assertEquals(data.search(nodesGGB).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1)),new  Evaluation(values.size(),sumValues));
		
		
		//add new rows to the tree
		
		List<String> nodesGGR = Arrays.asList("Germany","Green","Red");
		List<Integer> valuesRed = Arrays.asList(4,5);
		valuesRed.stream().forEach( value -> data.insert(nodesGGR, value));
		
		sumValues =  Stream.concat( valuesRed.stream(),values.stream()).mapToInt(Integer::intValue).sum();
		
		
		data.accept(new EvaluationVisitor(sum));
		
		
		assertEquals(data.search(nodesGGB.subList(0, 1)).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1)),new  Evaluation(values.size()+valuesRed.size(),sumValues));
		
		
		
		assertEquals(data.search(nodesGGR).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1)),new  Evaluation(valuesRed.size(),valuesRed.stream().mapToInt(Integer::intValue).sum()));
		
	}

}
