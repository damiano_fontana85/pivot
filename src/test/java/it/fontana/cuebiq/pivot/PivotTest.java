//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

package it.fontana.cuebiq.pivot;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import it.fontana.cuebiq.pivot.tree.Tree;
import it.fontana.cuebiq.pivot.tree.visitor.PrintVisitor;
import it.fontana.cuebiq.pivot.tree.visitor.Visitor;

public class PivotTest extends CommonTest {

	
	
	@Test(expected = IllegalArgumentException.class) 
	public void testBadRow() {
		Map<String,String> row1 = new HashMap<String, String>();
		row1.put("Nation","Germany");
		row1.put("eyes","Green");
		
		Row a = new Row(row1, 168);
		
		Map<String,String> row2 = new HashMap<String, String>();
		row2.put("Nation","Spain");
		row2.put("eyes","Green");
		row2.put("Hair","Brown");
		
		Row b = new Row(row2, 359);
		
		Pivot.getInstance().evaluateData(Arrays.asList(a,b),average, "Nation","eyes","Hair");
		
		
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testBadClassifiers() {
		Map<String,String> row1 = new HashMap<String, String>();
		row1.put("Nation","Germany");
		row1.put("eyes","Green");
		row1.put("Hair","Brown");
		
		
		Row a = new Row(row1, 168);
		
		Map<String,String> row2 = new HashMap<String, String>();
		row2.put("Nation","Spain");
		row2.put("eyes","Green");
		row2.put("Hair","Brown");
		
		Row b = new Row(row2, 359);
		
		Pivot.getInstance().evaluateData(Arrays.asList(a,b),average, "Nation","eyes","axx");
		
		
	}
	
	
	@Test
	public void testEvaluation() throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException {
		
		Map<String,String> row1 = new HashMap<String, String>();
		row1.put("Nation","Germany");
		row1.put("eyes","Green");
		row1.put("Hair","Brown");
		
		Row a = new Row(row1, 168);
		
		Map<String,String> row2 = new HashMap<String, String>();
		row2.put("Nation","Spain");
		row2.put("eyes","Green");
		row2.put("Hair","Brown");
		
		Row b = new Row(row2, 359);
		
		
		Map<String,String> row3 = new HashMap<String, String>();
		row3.put("Nation","Germany");
		row3.put("eyes","Blue");
		row3.put("Hair","Brown");
		
		Row c = new Row(row3, 489);
		
		
		Map<String,String> row4 = new HashMap<String, String>();
		row4.put("Nation","France");
		row4.put("eyes","Blue");
		row4.put("Hair","Black");
		
		Row d = new Row(row4, 506);
		
		
		Map<String,String> row5 = new HashMap<String, String>();
		row5.put("Nation","France");
		row5.put("eyes","Blue");
		row5.put("Hair","Black");
		
		Row e = new Row(row5, 498);
		
			
		
		
		Tree treeSum = Pivot.getInstance().evaluateData(Arrays.asList(a,b,c,d,e),sum, "eyes","Hair","Nation");
		
		Visitor<?> print = new PrintVisitor();
		treeSum.accept(print);
		
		System.out.println(print.getResult());
		
		Tree treeAverage = Pivot.getInstance().evaluateData(Arrays.asList(a,b,c,d,e),average, "eyes","Hair","Nation");
		
		
		Visitor<?> printAvg = new PrintVisitor();
		treeAverage.accept(printAvg);
		
		System.out.println(printAvg.getResult());
	
		
		//check sum
		Evaluation eval = treeSum.search(Arrays.asList("Green","Brown")).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1));
	
		List<Row> aggregated = groupListBy(Arrays.asList(a,b,c,d,e),"eyes","Hair").get(Arrays.asList("Green","Brown"));
	
		int sum = aggregated.stream().mapToInt(r -> r.getValue()).sum();
		
		assertEquals(new Evaluation(aggregated.size(), sum), eval);
		
		
		eval = treeSum.search(Arrays.asList("Blue","Black","France")).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1));
		
		aggregated = groupListBy(Arrays.asList(a,b,c,d,e),"eyes","Hair","Nation").get(Arrays.asList("Blue","Black","France"));
	
		sum = aggregated.stream().mapToInt(r -> r.getValue()).sum();
		
		assertEquals(new Evaluation(aggregated.size(), sum), eval);
		
		
		eval = treeSum.search(Arrays.asList("Blue")).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1));
		
		aggregated = groupListBy(Arrays.asList(a,b,c,d,e),"eyes").get(Arrays.asList("Blue"));
	
		sum = aggregated.stream().mapToInt(r -> r.getValue()).sum();
		
		assertEquals(new Evaluation(aggregated.size(), sum), eval);
	
	
		
		//check average
		
		eval = treeAverage.search(Arrays.asList("Green","Brown")).flatMap(l -> l.getEvaluation()).orElse(new Evaluation(-1, -1));
		
		aggregated = groupListBy(Arrays.asList(a,b,c,d,e),"eyes","Hair").get(Arrays.asList("Green","Brown"));
	
		double average = aggregated.stream().mapToInt(r -> r.getValue()).average().getAsDouble();
		
		assertEquals(new Evaluation(aggregated.size(), average), eval);
	
		
		eval = treeAverage.getEvaluation().orElse(new Evaluation(-1, -1));
		
		aggregated = groupListBy(Arrays.asList(a,b,c,d,e)).get(Collections.EMPTY_LIST);
	
		average = aggregated.stream().mapToInt(r -> r.getValue()).average().getAsDouble();
		
		assertEquals(new Evaluation(aggregated.size(), average), eval);

	}

}
