//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

package it.fontana.cuebiq.pivot;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommonTest {


	Function<List<Evaluation>, Evaluation> sum = r -> new Evaluation(r.stream().mapToLong(Evaluation::getCount).sum(), r.stream().mapToInt(x -> x.getValue().intValue()).sum());

	Function<List<Evaluation>, Evaluation> average = r -> {
		long count = r.stream().mapToLong(Evaluation::getCount).sum();
		double sum = r.stream().mapToDouble(x -> x.getValue().doubleValue()*new Double(x.getCount())).sum();
		return new Evaluation(count, sum/count );
	};


	protected Map<List<String>, List<Row>> groupListBy(List<Row> data, String ... groupByFieldNames) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException {
		
		return data.stream().collect(Collectors.groupingBy(
				r ->Stream.of(groupByFieldNames).map(f -> {
						return (String) r.getClassifiersValue(f);			
				}).collect(Collectors.toList()) ));
	}

}
