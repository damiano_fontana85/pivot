//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

package it.fontana.cuebiq.pivot;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import it.fontana.cuebiq.pivot.tree.Node;
import it.fontana.cuebiq.pivot.tree.Tree;
import it.fontana.cuebiq.pivot.tree.visitor.EvaluationVisitor;
import it.fontana.cuebiq.pivot.util.Assert;

public class Pivot {

	private static volatile Pivot instance;

	public static Pivot getInstance() {
		if (instance == null) {
			synchronized (Pivot.class) {
				if (instance == null) {
					instance = new Pivot();
				}
			}
		}
		return instance;
	}

	protected Pivot() {super();}


	/**
	 * @param data i dati classificati 
	 * @param fun la funzione usata per calcolare 
	 * @param aggregations la lista di aggregazioni
	 * @return l'albero di aggregazione 
	 */
	public Tree evaluateData(List<Row> data,Function<List<Evaluation>, Evaluation> fun,String ... aggregations) {
		Assert.notNull(data, "data must be defined");
		Assert.isTrue(aggregations.length != 0, "at least one aggregation must be defined");
		Assert.notNull(fun, "aggregation function must be defined");
		
		Tree tree = buildTree(data, aggregations);
		tree.accept(new EvaluationVisitor(fun));
		return tree;
	}



	/**
	 * Questo metodo crea l'albero di aggregazione a partire dalle colonne di aggregazione
	 * definite
	 * 
	 * @param data i dati classificati
	 * @param aggregations la lista di aggregazioni
	 * @return l'albero di aggregazione senza valori calcolati
	 */
	
	protected Tree buildTree(List<Row> data,String ... aggregation) {

		String key = "root";
		Tree tree = new Node(key,0);
		data.stream().map( row -> getPathFromAggregation(row, aggregation)).forEach( result -> tree.insert(result.classifiers, result.value));

		return tree;
	}



	protected  ClassifiersAndValue getPathFromAggregation(Row row, String ... aggregation){
		boolean rowContainsAllAggreagation = Stream.of(aggregation).allMatch( a ->  row.getClassifiers().containsKey(a));
		
		Assert.isTrue(rowContainsAllAggreagation, "row must contains all aggregations");
		
		List<String> classifiers = Stream.of(aggregation).map( a -> row.getClassifiers().get(a)).collect(Collectors.toList());
		return new ClassifiersAndValue(classifiers, row.getValue());
		
	}



	protected  class ClassifiersAndValue
	{
		public List<String> classifiers;
		public int value;

		public ClassifiersAndValue(List<String> classifiers, int value) {
			this.classifiers = classifiers;
			this.value = value;
		}

	}

}
