//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


package it.fontana.cuebiq.pivot.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import it.fontana.cuebiq.pivot.tree.visitor.Visitor;
import it.fontana.cuebiq.pivot.util.Assert;

public class Leaf extends Tree{

	private List<Integer> values = new ArrayList<Integer>();
 
	public Leaf(String key,int level) {
		super(key,level);
	}
	
	
	public List<Integer> getValues() {
		return values;
	}


	@Override
	public void accept(Visitor<?> v) {
		Assert.notNull(v, "visitor must be not null");
	
		v.visit(this);
		
	}
	
	@Override
	public void insert(List<String> nodes,int value) 
	{
		values.add(value);

	}
	
	
	@Override
	public Optional<Tree> search(List<String> nodes) {
		if(nodes == null || !nodes.isEmpty())
			return Optional.empty();

		return Optional.of(this);
			
	}

	

	@Override
	public Optional<Tree> getChild(String key) {
		return Optional.empty();
	}
	
	@Override
	public String toString() {
		return super.toString() +" -> "+ values.stream().map(i -> i.toString()).collect(Collectors.joining(" "))+" = "+getEvaluation().map(e -> ""+e.getValue()).orElse("");
		
	}




	


	
	


}
