//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

package it.fontana.cuebiq.pivot.tree;

import java.util.List;
import java.util.Optional;
import it.fontana.cuebiq.pivot.Evaluation;
import it.fontana.cuebiq.pivot.tree.visitor.Visitor;

public abstract class Tree {


	protected String key;
	
	protected int level;
	
	
	protected Evaluation evaluation;


	public Tree(String key,int level) {
		this.key = key;
		this.level = level;
	}
	
	
	public abstract void accept(Visitor<?> v);
	
	public abstract void insert(List<String> nodes,int value) ;

	public abstract Optional<Tree> search(List<String> nodes) ;

	
	public abstract Optional<Tree> getChild(String key);

	public String getKey() {
		return key;
	}
	
	public Optional<Evaluation> getEvaluation() {
		return Optional.ofNullable(evaluation);
	}
	
	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}



	public int getLevel() {
		return level;
	}


	@Override
	public String toString() {
		return key;
	}





}
