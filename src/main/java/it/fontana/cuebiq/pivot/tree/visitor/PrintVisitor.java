//  Copyright (c) 2018 Damiano Fontana
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

package it.fontana.cuebiq.pivot.tree.visitor;


import java.util.stream.Collectors;
import java.util.stream.IntStream;

import it.fontana.cuebiq.pivot.Evaluation;
import it.fontana.cuebiq.pivot.tree.Leaf;
import it.fontana.cuebiq.pivot.tree.Node;
import it.fontana.cuebiq.pivot.tree.Tree;

public class PrintVisitor implements Visitor<String> {

	protected static final int indent = 2;

	String line = "";
	
	
	@Override
	public void visit(Node node) {
		addLine(node);	
	}

	@Override
	public void visit(Leaf leaf) {
		addLine(leaf);	

	}
	
	protected void addLine(Tree tree) {
		String inc = IntStream.rangeClosed(1, indent*tree.getLevel()).mapToObj( i -> " ").collect(Collectors.joining());
		line = line +" "+(inc + tree)+"\n";

	}

	@Override
	public boolean isTopDown() {
		return true;
	}


	@Override
	public String getResult() {
		return line;
	}

}
